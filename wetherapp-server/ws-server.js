
var cron = require('node-schedule');

// optional. process title | ps, top
process.title = 'node-chat';

var wsServerPort = 1337;

// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');

// message history
var history = [ ];
// connected clients
var clients = [ ];

// escape input caracters
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;')
                      .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

// color array
var colors = [ 'red', 'green', 'blue', 'magenta', 'purple', 'plum', 'orange' ];
// random color select
colors.sort(function(a,b) { return Math.random() > 0.5; } );

// http server
var server = http.createServer(function(request, response) {
    
    if (request.url == "/ping") {
        
        response.writeHead(200, { 'Content-Type': 'text/html' });
        response.write('<html><body><p>Server is running...</p></body></html>');
        response.end();
    
    }
});
server.listen(wsServerPort, '0.0.0.0', function() {
    console.log((new Date()) + " Server is listening on port : " + wsServerPort);
});

// ws server http://tools.ietf.org/html/rfc6455#page-6
var wsServer = new webSocketServer({
    httpServer: server
});

// callback upon new connection
wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

    var connection = request.accept(null, request.origin); 
    var index = clients.push(connection) - 1;
    var userName = false;
    var userColor = false;

    console.log((new Date()) + ' Connection accepted.');

    // send back chat history
    if (history.length > 0) {
        connection.sendUTF(JSON.stringify( { type: 'history', data: history} ));
    }

    // user sent some message
    connection.on('message', function(message) {

        if (message.type === 'utf8') { 
            if (userName === false) { 
                userName = htmlEntities(message.utf8Data);
                userColor = colors.shift();
                connection.sendUTF(JSON.stringify({ type:'color', data: userColor }));
                console.log((new Date()) + ' User is known as: ' + userName
                            + ' with ' + userColor + ' color.');

            } else {
                console.log((new Date()) + ' Received Message from '
                            + userName + ': ' + message.utf8Data);
                
                var obj = {
                    time: (new Date()).getTime(),
                    text: htmlEntities(message.utf8Data),
                    author: userName,
                    color: userColor
                };
                history.push(obj);
                history = history.slice(-100);

                // broadcast message to all connected clients
                var json = JSON.stringify({ type:'message', data: obj });
                for (var i=0; i < clients.length; i++) {
                    clients[i].sendUTF(json);
                }
            }
        }
    });

    // user disconnected
    connection.on('close', function(connection) {
        if (userName !== false && userColor !== false) {
            console.log((new Date()) + " Peer "
                + connection.remoteAddress + " disconnected.");
            clients.splice(index, 1);
            colors.push(userColor);

        }
    });

});

// schdular - send a constant object in 30 seconds time
var rule = new cron.RecurrenceRule();
rule.second = 30;

cron.scheduleJob(rule, function(){

    var cronTime = new Date().getTime();

    var wetherUpdate = {
        time: cronTime,
        text: htmlEntities('<p>sample werher update ' + cronTime + '</p>'),
    };

    var wetherUpdateJson = JSON.stringify({ type:'wether', data: wetherUpdate });

    console.log(new Date(), 'The 30th second of the minute.');

    console.log('--> Wether update   : ' + wetherUpdateJson);
    console.log('--> Current clients : ' + clients.length);

    for (var i=0; i < clients.length; i++) {
        clients[i].sendUTF(wetherUpdateJson);
    }
});