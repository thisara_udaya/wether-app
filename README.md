# wether-app

Follow the following steps to turn up the appilication

1 Git clone project
2 install dependancies

wetherapp-client

- npm install babel-core babel-loader babel-preset-env babel-preset-react babel-preset-es2015 html-webpack-plugin
- npm install react react-dom
- npm install webpack webpack-cli webpack-dev-server
- npm install websocket

wetherapp-server

- npm install http
- npm install websocket
- npm install node-schedule


3 turn up server

 - node ws-server.js

4 turn up client

 a. ifconfig - get the server IP

 b. update the server IP in wsHost in ws/ws-client.js

 c. npm start
