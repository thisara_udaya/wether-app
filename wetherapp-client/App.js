import React, { Component } from 'react';
class App extends Component{
   constructor(){
      super();
      this.state = {
         data:
         [
            {
               "id" : 1,
               "name" : "test",
               "age" : 20
            }
         ]
      }
   }
   render(){
      return(
         <div className = 'wrapper'>

            <Head />
            <WhetherContenet />
            <ChatContent />
            <Foot />
            
         </div>
      );
   }
}
class Head extends Component{
   render(){
      return(
         <div className="box a">
            <h1>Wether Online</h1>
         </div>
      );
   }
}

class WhetherContenet extends Component{
   render(){
      return (
         <div id="wetherbox" className="box c"></div>
      );
   }
}

class ChatContent extends Component{
   render(){
      return(
            <div className="box b">
               <div id = "content"></div>
               <div id = "typeIn">
               <span id="status">Connecting...</span>
               <input type="text" id="input" disabled="disabled" />
               </div>
            </div>
      );
   }
}

class Foot extends Component{
   render(){
      return(
         <div className="box d">- sample application -</div>
      );
   }
}

export default App;