$(function () {

    // DOM updated components
    var content = $('#content');
    var input = $('#input');
    var status = $('#status');
    var wetherbox = $('#wetherbox');

    //Webscoket server info
    var wshost = '192.168.1.6';
    var wsport = '1337';

    // color assinged for a user
    var myColor = false;
    // my name sent to the server
    var myName = false;

    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    // if browser doesn't support WebSocket, just show some notification and exit
    if (!window.WebSocket) {
        content.html($('<p>', { text: 'Sorry, your browser doesn\'t support WebSockets.'} ));
        input.hide();
        $('span').hide();
        return;
    }

    // declare ws connection
    var connection = new WebSocket('ws://'+ wshost +':'+ wsport);

    // onopen enabled input field and allow user to delcare name 
    connection.onopen = function () {
        input.removeAttr('disabled');
        status.text('Choose name:');
    };

    connection.onerror = function (error) {
        content.html($('<p>', { text: 'Sorry, problem with your connection or the server is down.' } ));
    };

    // incoming messages from ws server
    connection.onmessage = function (message) {

        try {
            var json = JSON.parse(message.data);
        } catch (e) {
            console.log('JSON parsing error : ', message.data);
            return;
        }

        // first response from the server with user's color
        if (json.type === 'color') {
            myColor = json.data;
            status.text(myName + ': ').css('color', myColor);
            input.removeAttr('disabled').focus();
        
        // populate new messages from other users on chat window
        } else if (json.type === 'history') {
            for (var i=0; i < json.data.length; i++) {
                addMessage(json.data[i].author, json.data[i].text, json.data[i].color, new Date(json.data[i].time));
            }
        // add new message to chat window
        } else if (json.type === 'message') {
            input.removeAttr('disabled');
            addMessage(json.data.author, json.data.text, json.data.color, new Date(json.data.time));
        // update wether information from ws server via schduler
        } else if (json.type === 'wether'){
            addWether(json.data.text, new Date(json.data.time)); //** TODO : wether not updating on wetherbox div
        }else {
            console.log('Unknown JSON type : ', json.type);
        }
    };


    // Send mesage to ws server when user presses Enter key
    input.keydown(function(e) {
        if (e.keyCode === 13) {
            var msg = $(this).val();
            if (!msg) {
                return;
            }

            connection.send(msg);
            $(this).val('');
            input.attr('disabled', 'disabled');

            if (myName === false) {
                myName = msg;
            }
        }
    });

    // Notify all users when a user leaves chat
    window.addEventListener('beforeunload', (event) => {
        let message = myName + ' is left the chat.';
        connection.send(message);
    });
      

    // If the server does not respond in 2 seconds, display error 
    setInterval(function() {
        if (connection.readyState !== 1) {
            status.text('Error');
            input.attr('disabled', 'disabled').val('Communication error.');
        }
    }, 2000);

    // compose chat message
    function addMessage(author, message, color, dt) {

        content.scrollTop = content.scrollHeight;

        let chatMessage = '<p><span style="color:' + color + '">' + author + '</span> @ ' +
        + (dt.getHours() < 10 ? '0' + dt.getHours() : dt.getHours()) + ':'
        + (dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes())
        + ': ' + message + '</p>'; 

        content.append(chatMessage);
    }

    // compose wether forcast information
    function addWether(message, dt) {

        let wetherUpdate = '<p><span style='(dt.getHours() < 10 ? '0' + dt.getHours() : dt.getHours()) + ':'
        + (dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes())
        + ': ' + message + '</p>';

        wetherbox.append(wetherUpdate);
    }
});